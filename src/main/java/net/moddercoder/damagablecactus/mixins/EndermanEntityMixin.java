package net.moddercoder.damagablecactus.mixins;

import net.minecraft.block.Blocks;

import net.minecraft.entity.mob.EndermanEntity;

import net.minecraft.entity.damage.DamageSource;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(EndermanEntity.class)
public class EndermanEntityMixin {
	@Inject(method = "mobTick", at = @At("HEAD"))
	public void mobTick(CallbackInfo callback) {
		EndermanEntity self = (EndermanEntity) (Object) this;
		try {
			if (self.getCarriedBlock().isOf(Blocks.CACTUS))
				if (self.world.random.nextInt(3) == 1)
					self.damage(DamageSource.CACTUS, 1f);
		} catch (NullPointerException npe) {}
	}
}
