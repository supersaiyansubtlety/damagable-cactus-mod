package net.moddercoder.damagablecactus.reference;

public class Reference {
	public static final String VERSION = "0.1";
	public static final String AUTHOR = "moddercoder";
	public static final String MODID = "damagablecactus";
	
	//Not recommended greater ~20 ticks. (Can be a phantom damage)
	public static final int RAYCAST_TIME_TICKS = 5;
	//Optimal default player reach distance (Can't be a dynamically!)
	public static final float REACH_DISTANCE = 5.0f;
}
